const http = require("http");

let Service, Characteristic;

async function timeoutPromise(ms){
  return await new Promise(r => setTimeout(r, ms));
}

module.exports = function(homebridge){
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;

  homebridge.registerPlatform("milight-esp-hub", "MiLightEsp", MilightPlatform);
}

class MilightPlatform{
  constructor(log, config){
    this._accessories = [];
    this.ip = config.ip;
    this.log = log;
    this.config = config;

    let aliases = config.aliases || [];
    let groups = config.groups || [];
    this.groupStates = [];
    for(const group of groups){
      let groupState = {
        state: new LightState(group.mainAlias),
        aliases: [],
        childStates: [],
      }
      this.groupStates.push(groupState);
      for(const alias of group.aliases){
        aliases.push(alias);
        groupState.aliases.push(alias);
      }
    }
    //remove duplicates
    aliases = [...new Set(aliases)];

    for(const alias of aliases){
      let accessory = new MilightAccessory(alias, this);
      this._accessories.push(accessory);
      for(const group of this.groupStates){
        if(group.aliases.includes(alias)){
          group.childStates.push(accessory.lightState);
        }
      }
    }

    this.isUpdatingAllStates = false;
    this.onUpdateLightStatesEndCbs = [];

    this.isDoingUpdateLoop = false;
    this.stateIntervalRequiresUpdate = false;

    setInterval(_ => {
      http.get("http://"+this.ip+"/gateways/").on("error", function (){console.log("GET request error")});
    }, 1000*30);
  }

  getServices(){
    return [];
  }

  accessories(cb){
    if(this._accessories.length>0){
      cb(this._accessories);
    }else{
      //if no alias list was given, try and receive from hub
      let self = this;
      self.readHubSettings().then(function(response){
        if(response){
          let settings = JSON.parse(response);
          for(var name in settings.group_id_aliases) {
            //TODO: use values to set light type?
            var values = settings.group_id_aliases[name];
            let accessory = new MilightAccessory(name, self);
            self._accessories.push(accessory);
            self.log("Added "+name);
          }
          cb(self._accessories);
        }
        else{
          //DO NOT fulfill callback here to block homebridge boot
          //cb(self._accessories);
          self.log("ERROR: Connecting to MiLight Hub falied, halting HomeBridge boot to avoid losing all configured lights...");
        }
      });
    }
  }

  async requestUpdateState(){
    this.stateIntervalRequiresUpdate = true;
    this.updateStatesLoop();
    return null;
  }

  async updateStatesLoop(){
    if(this.isDoingUpdateLoop) return;

    this.isDoingUpdateLoop = true;
    await timeoutPromise(200);
    this.updateAllStates();
    while(true){
      await timeoutPromise(100);
      if(this.stateIntervalRequiresUpdate){
        this.stateIntervalRequiresUpdate = false;
        this.updateAllStates();
      }else{
        break;
      }
    }
    this.isDoingUpdateLoop = false;
  }

  async updateAllStates(){
    //make sure this async function only has one instance running at a time
    while(this.isUpdatingAllStates){
      await new Promise(r => this.onUpdateLightStatesEndCbs.push(r));
    }
    this.isUpdatingAllStates = true;

    let ignoreAliases = [];
    let updateStates = [];
    let allSameGroups = [];

    //test if group states all have the same child values
    for(const group of this.groupStates){
      if(group.childStates.length <= 0) continue;
      let firstState = group.childStates[0];
      let allSame = true;
      for(const state of group.childStates){
        if(
          firstState.targetOn != state.targetOn ||
          firstState.targetBrightness != state.targetBrightness ||
          firstState.targetHue != state.targetHue ||
          firstState.targetSaturation != state.targetSaturation
        ){
          allSame = false;
          break;
        }
      }
      if(allSame){
        group.state.copyFromOtherTarget(firstState);
        allSameGroups.push(group);
        updateStates.push(group.state);
        ignoreAliases.push(...group.aliases);
      }else{
        group.state.flagAllInvalid();
      }
    }

    //add remaining accessory states
    for(const accessory of this._accessories){
      if(ignoreAliases.includes(accessory.alias)) continue;
      updateStates.push(accessory.lightState);
    }

    //update states
    let results = [];
    for(const state of updateStates){
      let beforeUpdateTime = Date.now()
      let result = await this.updateLightState(state);
      results.push(result);
      if(result){
        for(const group of allSameGroups){
          if(state == group.state){
            for(const childState of group.childStates){
              childState.copyFromOtherCurrent(state);
              childState.copyFromOtherTarget(state, beforeUpdateTime);
            }
          }
        }
      }
    }

    this.isUpdatingAllStates = false;
    let cbsCopy = [...this.onUpdateLightStatesEndCbs];
    this.onUpdateLightStatesEndCbs = [];
    for(const cb of cbsCopy){
      cb();
    }
    return results.every(v => v);
  }

  async readHubSettings(){
    // console.log("apiCall", alias, json);
    return await new Promise(resolve => {
      let url = "http://"+this.ip+"/settings";
      let req = http.request(url, {
        method: "GET"
      }, res => {
        let recvBody = "";
        res.on("data", chunk => {
          recvBody += chunk;
        });
        res.on("end", _ => {
          //console.log("response end, status: "+res.statusCode+" recvBody: "+recvBody);
          if(res.statusCode == 200){
            resolve(recvBody);
          }else{
            resolve(false);
          }
        });
      });
      req.on("error", e => {
        console.log("error sending to Milight esp hub", url, e);
        resolve(false);
      });
      req.end();
    });
  }

  async apiCall(alias, json){
    // console.log("apiCall", alias, json);
    return await new Promise(resolve => {
      let url = "http://"+this.ip+"/gateways/"+alias;
      let sendBody = JSON.stringify(json);
      let req = http.request(url, {
        method: "PUT",
        headers: {
          "Content-Length": sendBody.length,
        }
      }, res => {
        let recvBody = "";
        res.on("data", chunk => {
          recvBody += chunk;
        });
        res.on("end", _ => {
          // console.log("response end, status: "+res.statusCode+" recvBody: "+recvBody);
          if(res.statusCode == 200){
            resolve(true);
          }else{
            resolve(false);
          }
        });
      });
      req.on("error", e => {
        console.log("error sending to Milight esp hub", url, json, e);
        resolve(false);
      });
      req.write(sendBody);
      req.end();
    });
  }

  testUseKelvin(hue, saturation){
    if(hue < 150){
      let fn1 = -70 / (hue - 30) + 2.5;
      let fn2 = -70 / (hue - 33) + 1;
      return (saturation < fn1 || hue >= 30) && (saturation > fn2 && hue < 33);
    }else{
      let fn3 = 70 / (hue - 219) + 2.7;
      let fn4 = 90 / (hue - 216) + 0.8;
      return saturation < fn3 && saturation > fn4;
    }
  }

  async updateLightState(lightState){
    let newState = {};
    let apiCallResults = [];

    let didSendOnState = false;
    let targetOn = lightState.targetOn;
    let didSendBrightness = false;
    let targetBrightness = lightState.targetBrightness;
    let didSendHueSaturation = false;
    let targetHue = lightState.targetHue;
    let targetSaturation = lightState.targetSaturation;

    if(lightState.currentSaturation != lightState.targetSaturation || lightState.currentHue != lightState.targetHue || lightState.currentHueSaturationInvalid){
      let targetSaturation = lightState.targetSaturation;
      let targetHue = lightState.targetHue;
      let useKelvin = this.testUseKelvin(lightState.targetHue, lightState.targetSaturation);
      let prevUseKelvin = this.testUseKelvin(lightState.currentHue, lightState.currentSaturation);
      if(useKelvin != prevUseKelvin){
        await this.apiCall(lightState.alias, {
          brightness: 0
        });
        lightState.currentBrightnessInvalid = true;
      }
      if(useKelvin){
        let kelvin = 100;
        if(lightState.targetHue > 150){
          kelvin = 0.5 - lightState.targetSaturation / 40;
        }else{
          kelvin = Math.sqrt(lightState.targetSaturation*0.0033) + 0.5;
          kelvin = Math.min(1, Math.max(0, kelvin));
        }
        kelvin *= 100;
        let result = await this.apiCall(lightState.alias, {kelvin});
        apiCallResults.push(result);
        if(result){
          lightState.currentSaturation = targetSaturation;
          lightState.currentHue = targetHue;
          lightState.currentHueSaturationInvalid = false;
        }
      }else{
        newState.hue = lightState.targetHue;
        newState.saturation = lightState.targetSaturation;
        didSendHueSaturation = true;
      }
    }

    let forceSendBrightness = false;
    let forceBrightness = false;
    let forceBrightnessValue = 0;
    if(lightState.currentOn != lightState.targetOn || lightState.currentOnInvalid){
      newState.state = lightState.targetOn ? "on" : "off";
      didSendOnState = true;
      if(lightState.targetOn){
        forceSendBrightness = true;
      }else{
        forceBrightness = true;
        forceBrightnessValue = 0;
      }
    }
    if(lightState.currentBrightness != lightState.targetBrightness || lightState.currentBrightnessInvalid || forceSendBrightness || forceBrightness){
      didSendBrightness = true;
      if(forceBrightness){
        newState.brightness = forceBrightnessValue;
      }else{
        newState.brightness = Math.round(lightState.targetBrightness/100*255)
      }
    }
    if(Object.entries(newState).length > 0){
      let result = await this.apiCall(lightState.alias, newState);
      apiCallResults.push(result);
      if(result){
        if(didSendOnState){
          lightState.currentOn = targetOn;
          lightState.currentOnInvalid = false;
        }
        if(didSendBrightness){
          lightState.currentBrightness = targetBrightness;
          lightState.currentBrightnessInvalid = false;
        }
        if(didSendHueSaturation){
          lightState.currentHue = targetHue;
          lightState.currentSaturation = targetSaturation;
          lightState.currentHueSaturationInvalid = false;
        }
      }
    }
    let allSuccess = apiCallResults.every(v => v);
    return allSuccess;
  }
}

class LightState{
  constructor(alias){
    this.alias = alias;

    this.currentOn = false;
    this.currentOnInvalid = false;
    this.currentBrightness = 100;
    this.currentBrightnessInvalid = false;
    this.currentHue = 0;
    this.currentSaturation = 0;
    this.currentHueSaturationInvalid = false;

    this.targetOn = false;
    this.targetBrightness = 100;
    this.targetHue = 0;
    this.targetSaturation = 0;

    this.lastTargetOnUpdateTime = 0;
    this.lastTargetBrightnessUpdateTime = 0;
    this.lastTargetHueUpdateTime = 0;
    this.lastTargetSaturationUpdateTime = 0;
  }

  copyFromOtherTarget(lightState, maxAllowedTime = 0){
    if(this.lastTargetOnUpdateTime < maxAllowedTime || maxAllowedTime <= 0){
      this.targetOn = lightState.targetOn;
    }
    if(this.lastTargetBrightnessUpdateTime < maxAllowedTime || maxAllowedTime <= 0){
      this.targetBrightness = lightState.targetBrightness;
    }
    if(this.lastTargetHueUpdateTime < maxAllowedTime || maxAllowedTime <= 0){
      this.targetHue = lightState.targetHue;
    }
    if(this.lastTargetSaturationUpdateTime < maxAllowedTime || maxAllowedTime <= 0){
      this.targetSaturation = lightState.targetSaturation;
    }
  }

  copyFromOtherCurrent(lightState){
    this.currentOn = lightState.currentOn;
    this.currentBrightness = lightState.currentBrightness;
    this.currentHue = lightState.currentHue;
    this.currentSaturation = lightState.currentSaturation;
  }

  flagAllInvalid(){
    this.currentOnInvalid = true;
    this.currentBrightnessInvalid = true;
    this.currentHueSaturationInvalid = true;
  }
}

class MilightAccessory{
  constructor(alias, platform){
    if(platform.config.beautify_alias === true){
      this.name = alias.replace("_"," ").replace("-"," ");
      var arr = this.name.split(" ");
      var out = [];
      arr.forEach((item, index)=>{
        out.push(item.charAt(0).toUpperCase() + item.slice(1));
      });
      this.name = out.join(" ");
    }
    if(!(platform.config.prefix_alias === false)){
      this.name = "Milight "+alias;
    }
    this.alias = alias;
    this.services = [];

    this.lightState = new LightState(alias);

    let infoService = new Service.AccessoryInformation();
    infoService.setCharacteristic(Characteristic.Manufacturer, "MiLight");
    infoService.setCharacteristic(Characteristic.Model, "MiLight");
    infoService.setCharacteristic(Characteristic.SerialNumber, "1337");
    this.services.push(infoService);

    let lightbulbService = new Service.Lightbulb(this.name);
    this.services.push(lightbulbService);

    let onCharacteristic = lightbulbService.getCharacteristic(Characteristic.On);
    onCharacteristic.on("set", async (value, cb) => {
      // console.log("update on state: "+value+ " "+Date.now());
      this.lightState.targetOn = value;
      this.lightState.lastTargetOnUpdateTime = Date.now();
      cb(await platform.requestUpdateState());
    });

    let brightnessCharacteristic = lightbulbService.getCharacteristic(Characteristic.Brightness);
    brightnessCharacteristic.on("set", async (value, cb) => {
      // console.log("update brightness "+value);
      this.lightState.targetBrightness = value;
      this.lightState.lastTargetBrightnessUpdateTime = Date.now();
      cb(await platform.requestUpdateState());
    });

    let hueCharacteristic = lightbulbService.getCharacteristic(Characteristic.Hue);
    hueCharacteristic.on("set", async (value, cb) => {
      // console.log("update hue "+value);
      this.lightState.targetHue = value;
      this.lightState.lastTargetHueUpdateTime = Date.now();
      cb(await platform.requestUpdateState());
    });

    let saturationCharacteristic = lightbulbService.getCharacteristic(Characteristic.Saturation);
    saturationCharacteristic.on("set", async (value, cb) => {
      // console.log("update saturation "+value);
      this.lightState.targetSaturation = value;
      this.lightState.lastTargetSaturationUpdateTime = Date.now();
      cb(await platform.requestUpdateState());
    });
  }

  getServices(){
    return this.services;
  }
}
